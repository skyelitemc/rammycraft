## RC-Potato Scripts ##
Private RammyCraft Scripts

Scripters

- TomYaMee (Author)
- Atsukio
- Scorpix (Colour Coder)

## Current Scripts ##
- RC-PotatoClan (Cancelled)
- RC-PotatoTown
- RC-PotatoLock
- RC-PotatoChat
- RC-PotatoEssentials
- RC-PotatoLogin
- RC-PotatoNewbie
- RC-PotatoBomb [On Hold]
- RC-PotatoBounty
- RC-PotatoAchievements
- RC-PotatoBan
- RC-PotatoEconomy
- RC-PotatoSmelt
- RC-PotatoSocket

## Features ##
- Clan System (Cancelled)
- Chat System (Release)
- Essentials (Release)
- Login System (Release)
- Newbie System (Release)
- Potato Bomb Minigame (Pre-Alpha)[On Hold]
- Lock System (Release)
- Town System (Release)
- Bounty System (Release)
- Achievements System (Alpha)
- Ban System (Alpha)
- Economy System (Beta)
- AutoSmelt Enchantment (Release)
- Socketing System (Alpha)


## Links ##
- [RammyCraft Official Gaming Fan Page](https://www.facebook.com/TeamPotatoes "RammyCraft Official Gaming Fan Page") 
- [RammyCraft Discussion Group] (https://www.facebook.com/groups/RammyCraft "RammyCraft Discussion Group")